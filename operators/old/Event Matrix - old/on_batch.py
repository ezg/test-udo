import pandas as pd
import ezpackage

from io import BytesIO

import matplotlib.pyplot as plt

user_col = attributes['user_id']
event_col = attributes['event']
time_col = attributes['timestamp']

df.sort_values(by=time_col)

groups = ezpackage.matrix.matrix(
    df, max_steps=5, event_col=event_col, index_col=user_col, time_col=time_col, thresh=float(params['threshold']) / 100.0)

res = BytesIO()
plt.savefig(res, format='png', transparent=True)
self.res = res
