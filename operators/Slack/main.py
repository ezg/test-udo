import script
import pandas as pd
import numpy as np

df = pd.DataFrame(np.random.randint(0,100,size=(15, 4)), columns=list('ABCD'))
params = {}
attributes = {}
params["webhook_url"] = "https://hooks.slack.com/services/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"
params["title"] = "Checkout new data"
params["link"] = "https://google.com"
script.run(df, attributes, params)
