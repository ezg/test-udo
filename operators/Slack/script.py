from os import link
import pandas as pd
import requests

def run(df, attributes, params):
    url = params['webhook_url']
    title = params['title']
    link = params['link']

    myobj = {
        "blocks": [
            {
                "type": "header",
                "text": {
                    "type": "plain_text",
                    "text": title,
                    "emoji": True
                }
            },
            {
                "type": "section",
                "fields": [
                    {
                        "type": "mrkdwn",
                        "text": "<"+link+"|Link to data>"
                    }
                ]
            }
        ]
    }
    x = requests.post(url, json = myobj)
    
run(df, attributes, params)
