import script
import pandas as pd

df = pd.read_csv("tracks-anon.csv")
attributes = {}
attributes['user_id'] = ["USER_ID"]
attributes['event'] = ["EVENT"]
attributes['timestamp'] = ["TIMESTAMP"]

params = {}
params["threshold"] = 1
script.run(df, attributes, params)
