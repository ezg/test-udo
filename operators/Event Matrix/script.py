import pandas as pd
import ezpackage


def run(df, attributes, params):
    user_col = attributes['user_id'][0]
    event_col = attributes['event'][0]
    time_col = attributes['timestamp'][0]
    df.sort_values(by=time_col)
    groups = ezpackage.matrix.matrix(
        df, max_steps=5, event_col=event_col, index_col=user_col, time_col=time_col, thresh=float(params['threshold']) / 100.0)


run(df, attributes, params)
