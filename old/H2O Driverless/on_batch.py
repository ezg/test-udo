from getpass import getpass

import h2o_authn
import h2osteam
from h2osteam.clients import H2oKubernetesClient
from h2osteam.clients import DriverlessClient

from h2o.estimators.glm import H2OGeneralizedLinearEstimator
import h2o
from h2o.automl import H2OAutoML

import pandas as pd
import numpy as np

from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from matplotlib import pyplot

from io import BytesIO

CLIENT_ID = "hac-platform-public"
TOKEN_ENDPOINT = "https://auth.cloud.h2o.ai/auth/realms/hac/protocol/openid-connect/token"
REFRESH_TOKEN_URL = "https://cloud.h2o.ai/auth/get-platform-token"
REFRESH_TOKEN = params['refresh token']

H2O_STEAM_URL = "https://steam.cloud.h2o.ai/"

print(f"Visit {REFRESH_TOKEN_URL} to get your personal access token")
tp = h2o_authn.TokenProvider(
    refresh_token=REFRESH_TOKEN,
    client_id=CLIENT_ID,
    token_endpoint_url=TOKEN_ENDPOINT
)
steam = h2osteam.login(
    url=H2O_STEAM_URL,
    access_token=tp()
)

instance_name = params['cluster name']
found = False
for instance in steam.get_driverless_instances():
    print("===")
    print(instance["id"])
    print(instance["profile_name"])
    print(instance["status"])
    print(instance["name"])
    print(instance["version"])
    if instance_name == instance["name"]:
        if instance["status"] != "running":
            print("not running, will start again")
            DriverlessClient().get_instance(name=instance_name).start()
        found = True
        break

print("found instance, attempting to connect")
if found:
    dai = DriverlessClient().get_instance(name=instance_name).connect()
else:
    print("need to start")
    dai_machine = DriverlessClient().launch_instance(
        name=instance_name,
        version="1.10.3",
        profile_name="visitor-driverless-kubernetes",
        max_uptime_h=1
    )
    dai = dai_machine.connect()
print(dai.server.gui())
print(dai.server.docs("autoviz"))

df = df[attributes['features'] + attributes['target']]
df.to_csv("dataset.csv")

ds = dai.datasets.create("dataset.csv",
                         force=True
                         )

ds_split = ds.split_to_train_test(
    train_size=float(params['train size']) / 100.0)

settings = {
    'task': 'classification',
    'target_column': attributes['target'][0],
    'accuracy': params['accuracy'],
    'time': params['time'],
    'interpretability': params['interpretability'],
    'scorer': params['scorer']
}

ex = dai.experiments.create(**ds_split, **settings,
                            name='einblick-h2o', force=True)
# artifacts = ex.artifacts.download(overwrite=True)
# print(artifacts)
vi = ex.variable_importance()

# creating df object with columns specified
df = pd.DataFrame(vi.data, columns=vi.headers)

preds = ex.predict(ds_split['test_dataset'],
                   include_columns=[attributes['target'][0]])

test_predictions = pd.read_csv(preds.download("./"))

print(test_predictions.head())

test_predictions["Actual"] = np.where(
    test_predictions[attributes['target'][0]] == 1, 1, 0)

# calculate roc curves
ns_fpr, ns_tpr, _ = roc_curve(test_predictions["Actual"], [
    0 for _ in range(len(test_predictions[attributes['target'][0]]))])
lr_fpr, lr_tpr, _ = roc_curve(
    test_predictions["Actual"], test_predictions[attributes['target'][0] + ".1"])

# plot the roc curve for the model
pyplot.plot(ns_fpr, ns_tpr, linestyle='--', label='No Skill')
pyplot.plot(lr_fpr, lr_tpr, marker='.', label='Pipeline')

# axis labels
pyplot.xlabel('False Positive Rate')
pyplot.ylabel('True Positive Rate')
# show the legend
pyplot.legend()
# show the plot
res = BytesIO()
pyplot.savefig(res, format='png', transparent=True)
self.res = res

return df
