import h2o_authn
import h2osteam
from h2osteam.clients import H2oKubernetesClient

from h2o.estimators.glm import H2OGeneralizedLinearEstimator
import h2o
from h2o.automl import H2OAutoML

import pandas as pd
import numpy as np

CLIENT_ID = "hac-platform-public"
TOKEN_ENDPOINT = "https://auth.cloud.h2o.ai/auth/realms/hac/protocol/openid-connect/token"
REFRESH_TOKEN_URL = "https://cloud.h2o.ai/auth/get-platform-token"
REFRESH_TOKEN = params['refresh token']

H2O_STEAM_URL = "https://steam.cloud.h2o.ai/"

print(f"Visit {REFRESH_TOKEN_URL} to get your personal access token")
tp = h2o_authn.TokenProvider(
    refresh_token=REFRESH_TOKEN,
    client_id=CLIENT_ID,
    token_endpoint_url=TOKEN_ENDPOINT
)
steam = h2osteam.login(
    url=H2O_STEAM_URL,
    access_token=tp()
)

for instance in steam.get_h2o_kubernetes_clusters():
    print(instance["id"], "\t", instance["profile_name"],
          "\t", instance["status"], "\t", instance["name"])

cluster = H2oKubernetesClient().get_cluster(
    name=params['cluster name'], created_by=params['cluster owner'])
cluster.connect()

full_data = h2o.H2OFrame(df)
train, test = full_data.split_frame([0.8], seed=42)

y = attributes['target'][0]
x = attributes['features']

aml = H2OAutoML(max_runtime_secs=60,
                max_models=25,
                seed=42,
                project_name='classification',
                sort_metric="AUC")
aml.train(x=x, y=y, training_frame=train)

lb = aml.leaderboard
df = lb.as_data_frame()
return df
